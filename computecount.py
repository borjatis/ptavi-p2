import math
import sys


class Compute:
    import computeoochild

    def __init__(self):
        """ Initialize count """
        self.count = 0

    def _increment(self):
        """ Increment count """
        self.count += 1

    def power(self):
        self._increment()
        return self.num ** self.exp

    def log(self):
        self._increment()
        return math.log(self.num, self.base)

    def set_def(self):
        self._increment()

    def get_def(self):
        self._increment()
        return self.num

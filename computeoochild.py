#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Compute two math functions: raise to the power and logarithm.

The main code of the module takes operands and operation (raise or log)
from the command line, as arguments when running the program.

The format for calling the module from the command line is:
compute.py <operation> <num> [<num2>]
<num2> (exponent for power, base for log) is optional: it is
assumed to be 2 if not specified.

For example:
compute.py power 2 3
8.0
compute.py log 8 2
3.0
"""

import math
import sys

from compute import power, log

import computeoo


class ComputeChild(computeoo.Compute):
    pass

    def set_def(self):
        if self.exp is None:
            self.exp = 2
            sys.exit('Does not specify exp, by default the value will be 2')

        elif self.base is None:
            self.base = 2
            sys.exit('Does not specify base, by default the value will be 2')

        elif self.base < 0:
            raise ValueError('Number must be non-negative')

    def get_def(self):
        return self.num


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        result = power(num, num2)
    elif sys.argv[1] == "log":
        result = log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)
